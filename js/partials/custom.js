(function( $ ) {
    $(function() {

        // toggle menu icon
        $('.menu-icon').on('click', function () {
          $(this).addClass('opened');
        });

        // Headroom
        var myElement = document.querySelector(".header");
        var headroom  = new Headroom(myElement);
        headroom.init();

    });
    $(document).ready(function(){

        $(".menu-title").on('click', function(e){
            e.preventDefault();
            $('.main-menu-container').toggleClass("active").slideToggle();
            $('.nav-icon').toggleClass('open');
        });
        //scroll to anchors
        $('a[href*=#]:not([href=#])').click(function(e) {            
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');                    
                    if (target.length) {
                            $('html,body').animate({
                                    scrollTop: target.offset().top
                            }, 1000);
                            return false;
                    }
            }
        });
        // function animateArrow(){
        //     $(".anchor-to-section").addClass("animated bounce");
        //     setTimeout(function(){ $(".anchor-to-section").removeClass("animated bounce"); }, 2000);
        // }
        // setInterval(animateArrow, 5000);
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $('.single-banner').hover(
                function(){
                    $(this).find('.hover-state').addClass('hover-disp-y');
                },
                function(){
                    $(this).find('.hover-state').removeClass('hover-disp-y');
                }
            );
        }
        $(".btn-openModal").on('click', function(){
            $("#videoModal").show();
        });
        $(".modal .close").on('click', function(){
            $("#videoModal").hide();
            var iframe = $(this).find("iframe");
            var iframeSrc = iframe.attr("src");
            iframe.attr("src", iframeSrc);
        });
        
        $('.modal').on('click', function(event){            
            var target = $( event.target );               
            if ( $("#videoModal").is(':visible') ) {
                if( !target.is( ".modal-content" ) ) {
                    $("#videoModal").hide();
                    var iframe = $(this).find("iframe");
                    var iframeSrc = iframe.attr("src");
                    iframe.attr("src", iframeSrc);
                }                            
            }                
        });
        
        
    });
    $(window).load(function(){
        //$(".anchor-to-section").addClass("animated bounce");
        //setTimeout(function(){ $(".anchor-to-section").removeClass("animated bounce"); }, 2000);
        
    });
})(jQuery);