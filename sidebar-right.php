<?php
/**
 * The right sidebar containing the right widget area
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since Grunt Boilerplate 0.1.0
 * @author 2manydots
 */

if ( is_active_sidebar( 'sidebar-right' ) ) :
    dynamic_sidebar( 'sidebar-right' );
endif; ?>