<?php
/* Template name: News */

get_header();
?>

<main class="main">

  <div class="row">
    <div class="medium-12 columns">
           
        <?php
        $page_num = get_query_var('paged');
        //echo $page_num;
        $arg = array(
            'post_type'	     => 'news',
            'order'		     => 'DESC',
            'orderby'	     => 'date',
            //'posts_per_page' => -1,
            //'paged' => $page_num
        );
        $the_query = new WP_Query( $arg );
        if ( $the_query->have_posts() ) : $count_posts = 1;?>
            <div class="news-archive-section">
                <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                    $do_not_duplicate = $post->ID; ?>
                    <?php if( $count_posts == 1 ): ?>
                        <div class="row news-single-row">
                            <div class="large-6 medium-6 small-12 columns news-text-column">
                                <?php if( get_field("news_title") ): ?>
                                    <div class="row">                                    
                                        <div class="large-12 medium-12 small-12 columns">
                                            <div class="info-section">
                                                <div class="title">
                                                    <?php the_field("news_title"); ?>
                                                </div>
                                                <?php if( $excerpt = get_field("left_content_news") ): ?>
                                                    <div class="content-excerpt">
                                                        <div class="excerpt">
                                                            <?php echo wp_trim_words($excerpt, 44); ?>
                                                        </div>
                                                        <div class="read-more">
                                                            <a href="<?php the_permalink(); ?>">Read more ></a>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="large-6 medium-6 small-12 columns">
                                <?php if( have_rows("top_banner_image") ): ?>
                                    <?php while( have_rows("top_banner_image") ): the_row(); ?>
                                        <?php if( $image = get_sub_field("image") ): ?>
                                            <div class="img-wrapper" style="background-image:url(<?php echo $image; ?>);"></div>
                                            <!--<img src="<?php echo $image; ?>" alt="img-news">-->
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="row news-single-row">
                            <div class="large-6 medium-6 small-12 columns">
                                <?php if( have_rows("top_banner_image") ): ?>
                                    <?php while( have_rows("top_banner_image") ): the_row(); ?>
                                        <?php if( $image = get_sub_field("image") ): ?>
                                            <div class="img-wrapper" style="background-image:url(<?php echo $image; ?>);"></div>
                                            <!--<img src="<?php echo $image; ?>" alt="img-news">-->
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                            <div class="large-6 medium-6 small-12 columns news-text-column">
                                <?php if( get_field("news_title") ): ?>
                                    <div class="row">
                                        <div class="large-12 medium-12 small-12 columns">
                                            <div class="info-section">
                                                <div class="title">
                                                    <?php the_field("news_title"); ?>
                                                </div>
                                                <?php if( $excerpt = get_field("left_content_news") ): ?>
                                                    <div class="content-excerpt">
                                                        <div class="excerpt">
                                                            <?php echo wp_trim_words($excerpt, 44); ?>
                                                        </div>
                                                        <div class="read-more">
                                                            <a href="<?php the_permalink(); ?>">Read more ></a>
                                                        </div>
                                                    </div>                                                    
                                                <?php endif; ?>                                            
                                            </div>                                        
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>                            
                        </div>
                    <?php endif; ?>
                <?php $count_posts++; endwhile; ?>
            </div>
                            
            <div class="pag"><?php foundation_pagination($the_query); ?> </div>
        <?php endif; wp_reset_query(); ?>

    </div>
  </div>
</main>

<?php
get_footer();
