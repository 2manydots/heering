<?php
/* Template name: Contact */

get_header();
?>
<main class="main contact-page">

  <div class="row">
    <div class="medium-12 columns">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <div class="row">
            <div class="large-12 columns">
                <div class="content-wrapper">
                    <div class="content">
                        <?php the_content(); ?>
                    </div>                
                </div>                
            </div>
          </div><!-- row -->

          <div class="row">
            <div class="large-6 medium-6 small-12 columns formulier">
                <div class="content-wrapper">
                    <div class="content">
                        <?php the_field('left_side') ?>
                    </div>              
                </div>                
            </div>
            <div class="large-6 medium-6 small-12 columns right-part">
                <div class="content">
                    <?php the_field('right_side') ?>             
                </div>              
            </div>
          </div><!-- row -->

          <?php
        endwhile;
      endif;
      ?>

    </div>
  </div>
</main>
<?php
get_footer();
