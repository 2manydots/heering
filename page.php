<?php
/**
 * The template for displaying default pages
 *
 * @package WordPress
 * @subpackage tmd-wp-grunt
 * @since 0.1.0
 */

get_header(); ?>

    <main class="main">

        <?php //get_template_part('parts/breadcrumbs'); ?>

        <div class="row">
            <div class="medium-12 columns">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <div class="row">
                            <div class="medium-12 columns">
                                <?php get_template_part('parts/header-banner'); ?>
                                <?php get_template_part('parts/flex-content'); ?>
                            </div>
                        </div>

                    <?php endwhile;

                    //get_template_part('parts/pagination');

                else :

                    get_template_part('parts/no-content');

                endif; ?>

            </div>
        </div>
    </main>

<?php get_footer(); ?>