<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]>   <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]>   <html class="no-js lt-ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php if ( !is_front_page() ) { wp_title(''); echo " - "; } bloginfo('name'); ?></title>
    <?php wp_head(); ?>
    
    <?php if( $_SERVER['SERVER_NAME'] == 'www.heeringholland.com' ): ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-102968216-1', 'auto');
            ga('send', 'pageview');
        </script>
    <?php elseif( $_SERVER['SERVER_NAME'] == 'spanish.heeringholland.com' ): ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-102968216-2', 'auto');
            ga('send', 'pageview');
        </script>
    <?php elseif( $_SERVER['SERVER_NAME'] == 'usa.heeringholland.com' ): ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-102968216-3', 'auto');
            ga('send', 'pageview');
        </script>
    <?php endif; ?>
</head>
<body <?php body_class(); ?>>
    <?php 
        function getUserIP() {
            $client = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote = $_SERVER['REMOTE_ADDR'];
            if (filter_var($client, FILTER_VALIDATE_IP)) {
                $ip = $client;
            } 
            elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
                $ip = $forward;
            } 
            else {
                $ip = $remote;
            }
            return $ip;
        }
        $a           = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.getUserIP()));
        $countrycode = $a['geoplugin_countryCode'];
        $subdomain   = current(explode('.', $_SERVER['SERVER_NAME']));
        if (($countrycode == "US") && ($subdomain != "usa")) {
            //header('Location: http://usa.heeringholland.com/');
            ?>
            <script>
                window.location.href='http://usa.heeringholland.com';
            </script>
            <?php
        }
    ?>
    <header class="header">        
        <div class="top-header-container">
            <div class="lang-line">
                <div class="row">
                    <div class="large-6 medium-6 small-12 columns">
                        <div class="language-switch">
                            <?php if( have_rows("language_switch", "options") ): ?>
                                <?php while( have_rows("language_switch", "options") ): the_row(); ?>
                                    <?php
                                        $text = get_sub_field("text");
                                        $link = get_sub_field("link");
                                    ?>
                                    <?php if( $text && $link ): ?>
                                        <a href="<?php echo $link; ?>"><?php echo $text; ?></a>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="medium-12 columns">
                    <div class="top-header-container-inner">                        
                        <div class="menu-toggler">
                            <div class="menu-title">                                
                                <div class="nav-icon">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="home-nav-link">
                            <?php 
                                $header_home_link_text = get_field("header_home_link_text", "option");
                                $header_home_link = get_field("header_home_link", "option");
                            ?>                                    
                            <?php if( $header_home_link_text && $header_home_link ): ?>
                                <a href="<?php echo $header_home_link; ?>"><?php echo $header_home_link_text; ?></a>
                            <?php endif; ?>
                        </div>
                        <a href="<?= home_url(); ?>" rel="nofollow" class="logo-sp">
                            <?php new Sprite('logo'); ?>
                            <div class="yellow-line"></div>
                        </a>
                        <div class="contact-nav-link">
                            <?php 
                                $header_contact_link_text = get_field("header_contact_link_text", "option");
                                $header_contact_link = get_field("header_contact_link", "option");
                            ?>                                    
                            <?php if( $header_contact_link_text && $header_contact_link ): ?>
                                <a href="<?php echo $header_contact_link; ?>"><?php echo $header_contact_link_text; ?></a>
                            <?php endif; ?>
                            <div class="bird-icon"></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="top-header-menu-container">
            <div class="row">
                <div class="medium-12 columns">                    
                    <div class="main-menu-container">
                        <?php wp_nav_menu(array('menu_class' => 'main-nav', 'theme_location' => 'main-nav')) ?>
                    </div>                    
                </div>
            </div>
        </div>
    </header>