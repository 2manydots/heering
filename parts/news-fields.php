<div class="single-news-section">    
    <?php if( get_field("news_title") ): ?>
        <div class="row">
            <div class="large-12 medium-12 small-12 columns">
                <div class="title">
                    <?php the_field("news_title"); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if( get_field("left_content_news") ): ?>
        <div class="content-section">
            <div class="row">
                <div class="large-6 medium-6 small-12 columns">
                    <div class="left-part">
                        <div class="content">
                            <?php the_field("left_content_news"); ?>
                        </div>
                    </div>                
                </div>
                <?php if ( get_field('right_image_news') ): ?>
                    <div class="large-6 medium-6 small-12 columns">
                        <img src="<?php the_field('right_image_news'); ?>" alt="news-image">                    
                    </div>
                <?php endif; ?>            
            </div>
        </div>        
    <?php endif; ?>
</div>

