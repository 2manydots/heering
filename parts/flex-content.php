<?php if( have_rows("flex_content_sections") ): $count_flex_sections = 1; ?>
    <?php while ( have_rows("flex_content_sections") ): the_row();?>        
        <?php if( get_row_layout() == 'three_columns_section' ): ?>
            <div class="three_columns_section" id="section-<?php echo $count_flex_sections; ?>">
                <div class="row">
                    <?php if( have_rows("section") ): ?>
                        <?php while ( have_rows("section") ): the_row(); ?>
                            <div class="large-4 medium-6 small-12 columns">
                                <div class="single-section">
                                    <?php if( get_sub_field("title") ): ?>
                                        <div class="title">
                                            <?php the_sub_field("title"); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if( get_sub_field("content") ): ?>
                                        <div class="content">
                                            <?php the_sub_field("content"); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php
                                        $link_text = get_sub_field("link_text");
                                        $link_url = get_sub_field("link_url");;
                                    ?>
                                    <?php if( $link_text && $link_url ): ?>
                                        <div class="read-more">
                                            <a href="<?php echo $link_url; ?>"><?php echo $link_text; ?></a><span>></span>
                                        </div>                                    
                                    <?php endif; ?>
                                </div>                                
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php $count_flex_sections++; ?>
        <?php elseif( get_row_layout() == 'two_columns_section_banners' ): ?>
            <?php if( have_rows("section") ): ?>
                <div class="two-columns-section-banners" id="section-<?php echo $count_flex_sections; ?>">
                    <div class="row default-row">
                        <?php while ( have_rows("section") ): the_row(); ?>
                            <div class="small-12 medium-6 large-6 columns default-column">
                                <div class="single-banner">
                                    <?php if( get_sub_field("image") ): ?>
                                        <div class="inner-banner">
                                            <div class="inner-banner-background" style="background-image:url(<?php the_sub_field("image"); ?>);"></div>
                                            <div class="inner-banner-content">
                                                <div class="left-part">
                                                    <?php if( get_sub_field("title") ): ?>
                                                        <div class="title">
                                                            <?php the_sub_field("title"); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="right-part">
                                                    <?php if( get_sub_field("content") ): ?>
                                                        <div class="content hover-state">
                                                            <?php the_sub_field("content"); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php
                                                        $link_text = get_sub_field("link_text");
                                                        $link_url = get_sub_field("link_url");;
                                                    ?>
                                                    <?php if( $link_text && $link_url ): ?>
                                                        <div class="link-container hover-state">
                                                            <a href="<?php echo $link_url; ?>"><?php echo $link_text; ?></a>
                                                        </div>                                                    
                                                    <?php endif; ?>
                                                </div>                                                
                                            </div>
                                            <div class="background-gradient"></div>
                                            <?php if( $link_text && $link_url ): ?>
                                                <a class="global-link" href="<?php echo $link_url; ?>"></a>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>                                    
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>                                
            <?php endif; ?>
            <?php $count_flex_sections++; ?>
        <?php elseif( get_row_layout() == 'two_columns_section_content' ): ?>
            <div class="two-columns-section-content" id="section-<?php echo $count_flex_sections; ?>">
                <div class="row">
                    <div class="large-6 medium-6 small-12 columns">
                        <?php if( get_sub_field("left_title") ): ?>
                            <div class="title">
                                <?php the_sub_field("left_title");?>
                            </div>
                        <?php endif; ?>
                        <?php if( get_sub_field("left_content") ): ?>
                            <div class="content">
                                <?php the_sub_field("left_content");?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="large-6 medium-6 small-12 columns">
                        <?php if( get_sub_field("right_title") ): ?>
                            <div class="title">
                                <?php the_sub_field("right_title");?>
                            </div>
                        <?php endif; ?>
                        <?php if( get_sub_field("right_content") ): ?>
                            <div class="content">
                                <?php the_sub_field("right_content");?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php $count_flex_sections++; ?>
        <?php elseif( get_row_layout() == 'specification_section' ): ?>
            <div class="specification-section" id="section-<?php echo $count_flex_sections; ?>">
                <div class="row">
                    <div class="large-6 medium-6 small-12 columns">
                        <div class="left-part">
                            <?php if( get_sub_field("product_specification_title") ): ?>
                                <div class="title">
                                    <h3><?php the_sub_field("product_specification_title"); ?></h3>
                                </div>
                            <?php endif; ?>
                            <?php if( have_rows("product_specification") ): ?>
                                <div class="product-specification-list">
                                    <?php while( have_rows("product_specification") ): the_row(); ?>
                                        <div class="product-specification">
                                            <div class="row">
                                                <div class="large-6 medium-6 small-6 columns">
                                                    <?php if( get_sub_field("left_text") ): ?>
                                                        <div class="text-col">
                                                            <?php the_sub_field("left_text"); ?>
                                                        </div>                                                    
                                                    <?php endif; ?>
                                                </div>
                                                <div class="large-6 medium-6 small-6 columns">
                                                    <?php if( get_sub_field("right_text") ): ?>
                                                        <div class="text-col">
                                                            <?php the_sub_field("right_text"); ?>
                                                        </div>                                                    
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>                                    
                                    <?php endwhile; ?>
                                </div>                                
                            <?php endif; ?>
                            <?php if( get_sub_field("content_left") ): ?>
                                <div class="content">
                                    <?php the_sub_field("content_left"); ?>
                                </div>
                            <?php endif; ?>
                        </div>                        
                    </div>
                    <div class="large-6 medium-6 small-12 columns">
                        <div class="right-part">
                            <?php if( get_sub_field("content_right") ): ?>
                                <div class="content">
                                    <?php the_sub_field("content_right"); ?>
                                </div>
                            <?php endif; ?>
                        </div>                        
                    </div>
                </div>
            </div>
            <?php $count_flex_sections++; ?>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>


