<?php if( have_rows("top_banner_image") ): ?>
    <?php while( have_rows("top_banner_image") ): the_row(); ?>
        <?php 
            $video_link_popup = get_sub_field("video_link_popup");
            $video_embed = get_sub_field("video_embed");
        ?>
        <?php if( $image = get_sub_field("image") ): ?>

            <?php if( $mobile_image = get_sub_field("mobile_image") ): ?>
                <?php $imageMobUrl = $mobile_image; ?>
            <?php else: ?>
                <?php $imageMobUrl = $image; ?>
            <?php endif; ?>

            <style scoped>
                .top_banner_image { background-image:url(<?php echo $image; ?>); }
                @media only screen and (max-width : 640px) { .top_banner_image { background-image:url(<?php echo $imageMobUrl; ?>); } }
            </style>

            <div class="top_banner_image">
                <?php 
                    $link_text = get_sub_field('link_text');
                    $link_url = get_sub_field('link_url');
                ?>
                <?php if( $top_left_text = get_sub_field("top_left_text") ): ?>
                    <div class="top-left-text">
                        <?php echo $top_left_text; ?>
                    </div>
                <?php endif; ?>
                <?php if( $center_caption_text = get_sub_field("center_caption_text") ): ?>
                    <div class="center-caption-text">
                        <?php echo $center_caption_text; ?>
                        <?php if( $link_text && $link_url ): ?>
                            <div class="read-more">
                                <?php if( $video_link_popup && $video_embed ): ?>
                                    <a class="btn-openModal" href="#"><?php echo $link_text; ?><span>></span></a>
                                    <!-- The Modal -->
                                    <div id="videoModal" class="modal">

                                      <!-- Modal content -->
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <span class="close">×</span>
                                          </div>                                        
                                        <?php echo $video_embed; ?>
                                      </div>

                                    </div>
                                <?php else: ?>
                                    <a href="<?php echo $link_url; ?>"><?php echo $link_text; ?><span>></span></a>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>                        
                    </div>
                <?php endif; ?>

                <?php if( $right_bottom_text = get_sub_field("right_bottom_text") ): ?>
                    <div class="right-bottom-text">
                        <p><?php echo $right_bottom_text; ?><p>
                    </div>
                <?php endif; ?>
                <div class="yellow-line"></div>
                <?php if( $anchor_section_number = get_sub_field("anchor_section_number") ): ?>
                    <div class="anchor-to-section">
                        <a href="#section-<?php echo $anchor_section_number; ?>"></a>
                    </div>
                <?php endif;  ?>
                
            </div>            
            <div class="news-navigation">
                <?php if( is_singular( 'news' ) ): ?>
                    <?php previous_post_link('%link', '< Previous post'); ?>
                    <?php next_post_link('%link', 'Next post >'); ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>    
<?php endif; ?>