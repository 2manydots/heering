<footer>
    <div class="row">
        <div class="large-12 columns">
            <div class="footer-inner-container">
                <div class="row">
                    <div class="small-12 medium-6 large-6 columns left-part">
                        <?php if( get_field("footer_left_part", "option") ): ?>
                            <div class="content">
                                <?php the_field("footer_left_part", "option"); ?>
                            </div>                            
                        <?php endif; ?>                        
                        <div class="yellow-line"></div>
                    </div>
                    <div class="small-12 medium-6 large-6 columns">
                        <div class="right-part">
                            <div class="address-menu-container">
                                <div class="address">
                                    <?php if( get_field("footer_address", 'option') ): ?>
                                        <?php the_field("footer_address", 'option'); ?>
                                    <?php endif; ?>
                                </div>
                                <div class="menu">
                                    <?php wp_nav_menu(array('menu_class' => 'footer-nav', 'theme_location' => 'footer-nav')) ?>
                                </div>
                            </div>
                            <div class="contact-container">
                                <?php if( get_field("footer_contact_us_title", "option") ): ?>
                                    <div class="title">
                                        <?php the_field("footer_contact_us_title", "option"); ?>
                                    </div>                                                        
                                <?php endif; ?>
                                <?php if( get_field("footer_contact_us_content", "option") ): ?>
                                    <div class="content">
                                        <?php the_field("footer_contact_us_content", "option"); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-12 large-12 columns">
                        <div class="socials">
                            <?php if( get_field("footer_social_in", 'option') ): ?>
                                <a class="footer-social-in" href="<?php the_field("footer_social_in", "option"); ?>" target="_blank"></a>
                            <?php endif; ?>
                            <div class="separator"></div>
                            <?php if( get_field("footer_wet", 'option') ): ?>
                                <a class="footer-wet" href="<?php the_field("footer_wet", "option"); ?>" target="_blank"></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <?php if( get_field("footer_copyright", 'option') ): ?>
                    <p><?php the_field("footer_copyright", "option"); ?></p>
                <?php endif; ?>
            </div>
        </div>        
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>